import javax.swing.JOptionPane;

public class Uso_Bucle_For2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int arroba=0;
		
		boolean punto = false;
		
		boolean terminacion = false;
		
		boolean comienzo = false;
		
		String mail=JOptionPane.showInputDialog("Introduce email: ");
		
		for(int i=0;i<mail.length();i++) {
			
			if (mail.charAt(i)=='@') {
				
				arroba++;
			}
			
			if(mail.charAt(i)=='.') {
				
				punto=true;
			}
			
			if(mail.endsWith(".es") || mail.endsWith(".com")) {
				
				terminacion=true;
			}
			
			if(mail.startsWith(".") || mail.startsWith(".")) {
				
				comienzo=true;
			}
			
		}
		
		if (arroba==1 && punto==true && terminacion==true && comienzo==false) {
			
			System.out.println("El email es correcto");
		}
		
		else{
			
			System.out.println("El email no es correcto");
				
		}

	}

}
