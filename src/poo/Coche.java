package poo;

public class Coche {
	
	private int ruedas;
	private int largo;
	private int ancho;
	private int motor;
	private int peso_plataforma;
	
	private String color;
	private int peso_total;
	private boolean asientos_cuero,climatizador;
	
	
	public Coche() {
		
		ruedas=4;
		largo=2000;
		ancho=300;
		motor=1600;
		peso_plataforma=500;
	}
	
	public String dime_largo() {//M�todo getter
		
		return "El largo del coche es " + largo;
	}
	
	public void establece_color(String color_coche) {//metodo setter
		
		color=color_coche;
		
		//color="Azul"; 
	}
	
	public String dime_color() { //Metodo getter
		
		return("El color del coche es " + color);
	}

	public String dime_datos_generales() {
		return"La plataforma del veh�culo tiene " + ruedas + " ruedas " + 
	" Mide " + largo/1000 + "m de largo y " + ancho + "cm de ancho y un peso de plataforma de " + peso_plataforma + "kg";
	}
	
	public void configura_asientos(String asientos_cuero) {		//Setter
		
		if(asientos_cuero=="si") {
			
			this.asientos_cuero=true;
		}else {
			this.asientos_cuero=false;
			
		}
	}
		
	public String dime_asientos() { 		//Getter
		
		if(asientos_cuero==true) {
			
			return "El coche tiene asientos de cuero";
		}else {
			return "El coche no tiene asientos de cuero";
		}
	
	}
}
