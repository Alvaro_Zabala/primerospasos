package poo;

import java.util.Date;
import java.util.GregorianCalendar;

public class Uso_Persona {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Persona[] lasPersonas= new Persona[2];
		
		lasPersonas[0]= new Empleado2("Alvaro",52000,1992,8,4);
		lasPersonas[1]= new Alumno("Pedro","Ciencias");
		
		for(Persona p: lasPersonas) {
			
			System.out.println(p.dame_nombre() + " , " + p.dameDescripcion());
		}
		
	}

}

abstract class Persona{
	
	public Persona(String nom) {
		
		nombre=nom;
	}
	
	private String nombre;
	
	public String dame_nombre() {
		
		return nombre;
	}
	
	public abstract String dameDescripcion();
	
	
	
}

class Empleado2 extends Persona{
	
	public Empleado2(String nom,double sue,int agno,int mes, int dia) {
		
		super(nom);
		sueldo=sue;
		GregorianCalendar calendario=new GregorianCalendar(agno,mes-1,dia);
		altaContrato=calendario.getTime();
		++IdSiguiente;
		id=IdSiguiente;
		
	}
	
	public String dameDescripcion() {
		
		return "Este empleado tiene un id: " + id + " con un sueldo: " + sueldo;
	}
	
	public double dameSueldo() {
		
		return sueldo;
	}
	
	public Date dameFechaContrato() {
		
		return altaContrato;
	}
	
	public void subeSueldo(double porcentaje) {
		
		double aumento=sueldo*porcentaje;
		sueldo+=aumento;
	}
	
	private double sueldo;
	private Date altaContrato;
	private static int IdSiguiente;
	private int id;
}

class Alumno extends Persona{
	
	public Alumno(String nom,String car){
		
		super(nom);
		carrera=car;
	}
	
	private String carrera;
	
	public String dameDescripcion() {
		
		return "El alumno est� estudiando " + carrera;
	}
	
}
	
