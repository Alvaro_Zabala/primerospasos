package poo;

import java.util.*;

public class Uso_Empleado {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Jefatura jefe_RRHH= new Jefatura("Alvaro",25000,1996,1,1);
		
		jefe_RRHH.estableceIncentivo(55000);
		
		
		Empleado[] misEmpleados=new Empleado[6];
		
		misEmpleados[0]=new Empleado("Ana",30000,2000,7,7);
		misEmpleados[1]=new Empleado("Carlos",50000,1995,6,15);
		misEmpleados[2]=new Empleado("Paco",25000,2005,9,25);
		misEmpleados[3]=new Empleado("Antonio",47500,2009,11,9);
		misEmpleados[4]= jefe_RRHH;
		misEmpleados[5]=new Jefatura("Luis",87500,2008,12,9);
		Jefatura jefa_Finanzas= new Jefatura("Sandra",52000,1985,2,3);
		
		System.out.println(jefa_Finanzas.tomar_decisiones("dar mas dias de vacaciones"));
		
		System.out.println("El jefe " + jefa_Finanzas.dameNombre() + " tiene un bonus de: " + jefa_Finanzas.establece_bonus(500));
		System.out.println(misEmpleados[3].dameNombre() + " tiene un bonus de: " + misEmpleados[3].establece_bonus(250));
		
		for(Empleado e:misEmpleados) {
			
			e.subeSueldo(5);
		}
		
		Arrays.sort(misEmpleados);
		
		for(Empleado e: misEmpleados) {
			
			System.out.println("Nombre: " + e.dameNombre() + " Sueldo: " + e.dameSueldo()
			+ " Fecha Alta: " + e.dameFechaContrato());
		}

	}

}

class Empleado implements Comparable,trabajadores{
	
		public Empleado(String nom,double sue,int agno,int mes, int dia) {
			
			nombre=nom;
			sueldo=sue;
			GregorianCalendar calendario=new GregorianCalendar(agno,mes-1,dia);
			altaContrato=calendario.getTime();
			++IdSiguiente;
			id=IdSiguiente;
			
		}
		
		public double establece_bonus(double gratificacion) {
			
			return trabajadores.bonus_base+gratificacion;
		}
		
		public Empleado(String nom) {
			
			this(nom,30000,2000,01,01);
		}
		
		public String dameNombre() {
			
			return nombre + " ID: " + id;
		}
		
		public double dameSueldo() {
			
			return sueldo;
		}
		
		public Date dameFechaContrato() {
			
			return altaContrato;
		}
		
		public void subeSueldo(double porcentaje) {
			
			double aumento=sueldo*porcentaje/100;
			sueldo+=aumento;
		}
		
		public int compareTo(Object miobjeto) {
			
			Empleado otroEmpleado=(Empleado) miobjeto;
			
			if(this.id<otroEmpleado.id) {
				
				return -1;
			}
			
			if(this.id>otroEmpleado.id) {
				
				return 1;
			}
			
			return 0;
		}
		
		private String nombre;
		private double sueldo;
		private Date altaContrato;
		private static int IdSiguiente;
		private int id;
}

class Jefatura extends Empleado implements Jefes{
	
	
	
	public Jefatura(String nom,double sue,int agno,int mes,int dia) {
		
		super(nom,sue,agno,mes,dia);
	}
	
	public String tomar_decisiones(String decision) {
		
		return "Un miembro de la direccion ha tomado la decision de: " + decision;
	}
	
	public double establece_bonus(double gratificacion) {
		
		double prima=2000;
		return trabajadores.bonus_base+gratificacion+prima;
	}
	public void estableceIncentivo(double b) {
		
		incentivo=b;
	}
	
	public double dameSueldo() {
		
		double sueldoJefe=super.dameSueldo();
		
		return sueldoJefe+incentivo;
	}
	
	private double incentivo;
	
	
}
