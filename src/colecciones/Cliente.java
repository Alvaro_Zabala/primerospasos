package colecciones;

public class Cliente {
	
	public Cliente(String nombre,String cuenta, double saldo) {
		
		this.nombre=nombre;
		this.cuenta=cuenta;
		this.saldo=saldo;
	}
	
	public String getNombre() {
		
		return nombre;
	}
	
	public String getCuenta() {
		
		return cuenta;
	}
	
	public double saldo() {
		
		return saldo;
	}
	
	public void setNombre(String nombre) {
		
		this.nombre=nombre;
	}
	
public void setCuenta(String cuenta) {
		
		this.cuenta=cuenta;
	}

public void setSaldo(double saldo) {
	
	this.saldo=saldo;
}

public double getSaldo() {
	
	return saldo;
}
	


	@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((cuenta == null) ? 0 : cuenta.hashCode());
	return result;
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Cliente other = (Cliente) obj;
	if (cuenta == null) {
		if (other.cuenta != null)
			return false;
	} else if (!cuenta.equals(other.cuenta))
		return false;
	return true;
}



	private String nombre;
	private String cuenta;
	private double saldo ;

}
