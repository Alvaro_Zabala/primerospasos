package colecciones;
import java.util.*;

public class PruebaTreeSet {

	public static void main (String [] args) {
		
		Articulo articulo1 = new Articulo(1,"Zapatillas");
		Articulo articulo2 = new Articulo(20,"Calcetines");
		Articulo articulo3 = new Articulo(3,"Botas");
		
		TreeSet<Articulo> ordenaArticulos= new TreeSet<Articulo>();
		ordenaArticulos.add(articulo1);
		ordenaArticulos.add(articulo2);
		ordenaArticulos.add(articulo3);
		
		
		
		for(Articulo articulo : ordenaArticulos) {
			
		System.out.println(articulo.getDesc());
		}
	}
}

class Articulo implements Comparable<Articulo>{

	public Articulo(int num,String Desc) {
		
		this.num=num;
		descripcion=Desc;
	}
		
	public String getDesc() {
			
			return descripcion;
		}
	
	public int compareTo(Articulo o) {
		// TODO Auto-generated method stub
		return num - o.num;
	}
	
	private int num;
	private String descripcion;
	
	
}
