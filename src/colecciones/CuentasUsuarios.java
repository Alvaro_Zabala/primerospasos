package colecciones;

import java.util.*;

public class CuentasUsuarios {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Cliente c1 = new Cliente("Juan","0001",50000);
		Cliente c2 = new Cliente("Pedro","0002",20000);
		Cliente c3 = new Cliente("Natalia","0003",40000);
		Cliente c4 = new Cliente("Luis","0004",60000);
		
		Set <Cliente> clientesBanco=new HashSet<Cliente>();
		
		clientesBanco.add(c1);
		clientesBanco.add(c2);
		clientesBanco.add(c3);
		clientesBanco.add(c4);
		
		/*for(Cliente cliente: clientesBanco) {
			
			System.out.println(cliente.getNombre() + " "
			+ cliente.getCuenta() + " " + cliente.getSaldo());
		}*/
		
		Iterator<Cliente> it;
		it=clientesBanco.iterator();
		
		while(it.hasNext()) {
			
			String nombreCliente=it.next().getNombre();
			
			System.out.println(nombreCliente);
		}
	}

}
