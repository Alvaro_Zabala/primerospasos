package graficos;
import java.awt.Toolkit;
import java.awt.*;

import javax.swing.*;

public class CreandoMarcoCentrado {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		MarcoCentrado mimarco=new MarcoCentrado();
		
		mimarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		mimarco.setVisible(true);
	}
	

}

class MarcoCentrado extends JFrame{
	
	public MarcoCentrado() {
		
		Toolkit mipantalla= Toolkit.getDefaultToolkit();
		Dimension tamanoPantalla=mipantalla.getScreenSize();
		
		int alturaPantalla=tamanoPantalla.height;
		int anchuraPantalla=tamanoPantalla.width;
		
		setSize(anchuraPantalla/2,alturaPantalla/2);
		setLocation(anchuraPantalla/4,alturaPantalla/4);
		
		setTitle("Marco Centrado");
		Image miIcono= mipantalla.getImage("icono.png");
		setIconImage(miIcono);
		
	}
	
	
	
}
