package graficos;
import javax.swing.*;
import java.awt.event.*;

public class Eventos_Ventana {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		MarcoVentana mimarco = new MarcoVentana();
		mimarco.setVisible(true);
		mimarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		MarcoVentana mimarco2 = new MarcoVentana();
		mimarco2.setVisible(true);
		mimarco2.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		mimarco.setTitle("MARCO 1");
		mimarco2.setTitle("MARCO 2");
		mimarco.setBounds(300,300,500,350);
		mimarco2.setBounds(900,300,500,350);
	}

}

class MarcoVentana extends JFrame{
	
	public MarcoVentana() {
		
		//setTitle("Mi marco");
		//setBounds(300,300,500,350);
		
		M_Ventana oyente = new M_Ventana();
		addWindowListener(oyente);
		
	}
	
	
}

class M_Ventana implements WindowListener{
	
	public void windowActivated(WindowEvent e) {
		
		System.out.println("Ventana activada");
	}
	public void windowClosed(WindowEvent e) {
		
		System.out.println("Ventana cerrada");
	}
	public void windowClosing(WindowEvent e) {
		
		System.out.println("Ventana cerrandose");
	}
	public void windowDeactivated(WindowEvent e) {
		
		System.out.println("Ventana desactivada");
	}
	public void windowDeiconified(WindowEvent e) {
		
		System.out.println("Ventana restaurada");
	}
	public void windowIconified(WindowEvent e) {
		
		System.out.println("Ventana minimizada");
		
	}
	public void windowOpened(WindowEvent e) {
		
		System.out.println("Ventana abierta");
	}
}
