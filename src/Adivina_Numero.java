import java.util.*;
public class Adivina_Numero {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int aleatorio =(int)(Math.random()*100);
		
		//System.out.println(aleatorio);el metodo random genera numeros aleatorios entre 0 y 1 de tipo double

		Scanner entrada=new Scanner(System.in);
		
		int numero = 0;
		int intentos = 0;
		
		while(numero!=aleatorio) {
			intentos++;
			System.out.println("Introduce un n�mero: ");
			
			numero =entrada.nextInt();
			
			if(aleatorio<numero) {
				
				System.out.println("M�s bajo");
			}
			
			else if(aleatorio>numero) {
				
				System.out.println("M�s alto");
			}
			
		}
		
		System.out.println("Correcto");
		System.out.println("Has ralizado "+ intentos + " intentos");
		
	}

}
