
public class Uso_Arrays_BidimensionalesII {
	
	public static void main(String[]args) {
	
	int [][] matriz={
		{10,21,25,51,48,78,96},
		{85,79,96,36,21,14,58},
		{23,12,13,65,58,82,34},
		{1,5,9,7,3,6,9}
	};
	
	for(int[]i:matriz) {
		
		System.out.println();
		
		for(int z:i) {
			
			System.out.print(z+ " ");
		}
	}
		
	
	}

}


