import java.util.*;
public class Notas_Alumnos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner entrada=new Scanner(System.in);
		
		System.out.println("Introduce el nombre del alumno ");
		String nombre=entrada.nextLine();
		
		double media;
		double[] notas=new double[3];
		
		for(int i=0;i<notas.length;i++) {
			
			System.out.println("Introduce la " + (i+1) +  " nota");
			notas[i]=entrada.nextDouble();	
		}
		
		media=(notas[0] + notas[1] + notas[2])/3;
		
		System.out.println("La media de las notas de " + nombre + " es: " + Math.round(media));
		
	}

}
