package areas;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class PruebaArea {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		MarcoArea mimarco = new MarcoArea();
		mimarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mimarco.setVisible(true);

	}

}

class MarcoArea extends JFrame{
	
	public MarcoArea() {
		
		setTitle("Area de texto");
		setBounds(500,300,500,350);
		setLayout(new BorderLayout());
		laminaBotones= new JPanel();
		boton1= new JButton("Insertar");
		boton1.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				areaTexto.append("En un lugar de la blablabla");
				
			}
				
		});
		
		laminaBotones.add(boton1);
		boton2=new JButton("Salto l�nea");
		boton2.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				
				boolean saltar =!areaTexto.getLineWrap();
				areaTexto.setLineWrap(saltar);
				
				if(saltar) {
					
					boton2.setText("Quitar salto");
				}else {
					
					boton2.setText("Salto linea");
				}
				
			}
			
		});
		
		laminaBotones.add(boton2);
		add(laminaBotones,BorderLayout.SOUTH);
		areaTexto= new JTextArea(8,20);
		laminaBarras= new JScrollPane(areaTexto);
		add(laminaBarras,BorderLayout.CENTER);
		
	}
	
	private JPanel laminaBotones;
	private JButton boton1;
	private JButton boton2;
	private JTextArea areaTexto;
	private JScrollPane laminaBarras;
	
}


