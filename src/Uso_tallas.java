import java.util.*;
public class Uso_tallas {
	
	//enum Talla{peque�a,mediana,grande,muy_grande};
	
	enum Talla{
		
		MINI("S"),MEDIANO("m"),GRANDE("l"),MUY_GRANDE("xl");
		
		private Talla(String abreviatura) {
			
			this.abreviatura=abreviatura;
		}
		
		private String abreviatura;
	
	
	public String dimeAbreviatura() {
		
		return abreviatura;
	}
	
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner entrada=new Scanner(System.in);
		
		System.out.println("Introduce una talla: MINI,MEDIANO,GRANDE,MUY_GRANDE");
		
		String entradaDatos=entrada.next().toUpperCase();
		
		Talla la_talla=Enum.valueOf(Talla.class, entradaDatos);
		
		System.out.println("Talla: " + la_talla);
		System.out.println("Abreviatura: " + la_talla.dimeAbreviatura());
		

		
		

	}

}
