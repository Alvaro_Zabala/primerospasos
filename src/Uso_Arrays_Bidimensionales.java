
public class Uso_Arrays_Bidimensionales {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int [][]matriz=new int[4][5];
		
		matriz[0][0]=12;
		matriz[0][1]=15;
		matriz[0][2]=18;
		matriz[0][3]=11;
		matriz[0][4]=10;
		
		matriz[1][0]=19;
		matriz[1][1]=21;
		matriz[1][2]=22;
		matriz[1][3]=32;
		matriz[1][4]=42;
		
		matriz[2][0]=82;
		matriz[2][1]=92;
		matriz[2][2]=72;
		matriz[2][3]=31;
		matriz[2][4]=35;
		
		matriz[3][0]=54;
		matriz[3][1]=45;
		matriz[3][2]=69;
		matriz[3][3]=96;
		matriz[3][4]=74;
		
		for(int i=0;i<4;i++) {
			System.out.println(" ");
			for (int j=0;j<5;j++) {
				
				System.out.print(matriz[i][j] + " ");
			}
		}

	}

}
