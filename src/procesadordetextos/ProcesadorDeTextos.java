package procesadordetextos;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ProcesadorDeTextos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		MarcoProcesador mimarco = new MarcoProcesador();
		mimarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mimarco.setVisible(true);

	}

}

class MarcoProcesador extends JFrame{
	
	public MarcoProcesador() {
		
		setTitle("Procesador de Textos");
		setBounds(500,300,550,400);
		LaminaProcesador laminaprinci = new LaminaProcesador();
		add(laminaprinci);
		
	}
	
class LaminaProcesador extends JPanel{
	
	public LaminaProcesador() {
		
		setLayout(new BorderLayout());
		JPanel laminamenu = new JPanel();
		JMenuBar mibarra = new JMenuBar();
		
		JMenu fuente = new JMenu("Fuente");
		JMenu estilo = new JMenu("Estilo");
		JMenu tamagno = new JMenu("Tama�o");
		
		JMenuItem arial=new JMenuItem("Arial");
		JMenuItem courier=new JMenuItem("Courier");
		JMenuItem verdana=new JMenuItem("Verdana");
		
		courier.addActionListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent e) {
				
				area.setFont(new Font("Courier",Font.PLAIN,12));
				
			}
		});
		arial.addActionListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent e) {
				
				area.setFont(new Font("Arial",Font.PLAIN,12));
				
			}
		});
		verdana.addActionListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent e) {
				
				area.setFont(new Font("Verdana",Font.PLAIN,12));
				
			}
		});
		
		fuente.add(arial);
		fuente.add(courier);
		fuente.add(verdana);
		JMenuItem negrita = new JMenuItem("Negrita");
		JMenuItem cursiva = new JMenuItem("Cursiva");
		estilo.add(negrita);
		estilo.add(cursiva);
		JMenuItem peque�o = new JMenuItem("10");
		JMenuItem mediano = new JMenuItem("14");
		JMenuItem grande = new JMenuItem("18");
		JMenuItem muygrande= new JMenuItem("22");
		tamagno.add(peque�o);
		tamagno.add(mediano);
		tamagno.add(grande);
		tamagno.add(muygrande);
		
		
		mibarra.add(fuente);
		mibarra.add(estilo);
		mibarra.add(tamagno);
		laminamenu.add(mibarra);
		add(laminamenu,BorderLayout.NORTH);
		area = new JTextPane();
		add(area,BorderLayout.CENTER);
		
		}
	
	JTextPane area;
	
	}
}
